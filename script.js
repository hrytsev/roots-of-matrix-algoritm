const a11 = parseFloat(document.getElementById("a11").value);
const a12 = parseFloat(document.getElementById("a12").value);
const a13 = parseFloat(document.getElementById("a13").value);
const b1 = parseFloat(document.getElementById("b1").value);

const a21 = parseFloat(document.getElementById("a21").value);
const a22 = parseFloat(document.getElementById("a22").value);
const a23 = parseFloat(document.getElementById("a23").value);
const b2 = parseFloat(document.getElementById("b2").value);

const a31 = parseFloat(document.getElementById("a31").value);
const a32 = parseFloat(document.getElementById("a32").value);
const a33 = parseFloat(document.getElementById("a33").value);
const b3 = parseFloat(document.getElementById("b3").value);
function roundTo(value, precision) {

    return Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);
}

function solveSystemByGauss() {
    const detA = a11 * a22 * a33 + a12 * a23 * a31 + a13 * a21 * a32
        - a13 * a22 * a31 - a12 * a21 * a33 - a11 * a23 * a32;

    const detA1 = b1 * a22 * a33 + a12 * a23 * b3 + a13 * b2 * a32
        - a13 * a22 * b3 - a12 * b2 * a33 - b1 * a23 * a32;

    const detA2 = a11 * b2 * a33 + b1 * a23 * a31 + a13 * a21 * b3
        - a13 * b2 * a31 - b1 * a21 * a33 - a11 * a23 * b3;

    const detA3 = a11 * a22 * b3 + a12 * b2 * a31 + b1 * a21 * a32
        - b1 * a22 * a31 - a12 * a21 * b3 - a11 * b2 * a32;

    const x1 = roundTo(detA1 / detA, 5);
    const x2 = roundTo(detA2 / detA, 5);
    const x3 = roundTo(detA3 / detA, 5);
    console.log(2)
    const solutionDiv = document.getElementById("solution");
    solutionDiv.innerHTML = "x1 = " + x1 + "<br>" +
        "x2 = " + x2 + "<br>" +
        "x3 = " + x3;
}
function solveSystemByIteration() {
    const eps = 0.5 * Math.pow(10, -5);
    const maxIterations = 1000;
    let x1 = 0;
    let x2 = 0;
    let x3 = 0;

    for (let i = 0; i < maxIterations; i++) {
        const newX1 = (b1 - a12 * x2 - a13 * x3) / a11;
        const newX2 = (b2 - a21 * x1 - a23 * x3) / a22;
        const newX3 = (b3 - a31 * x1 - a32 * x2) / a33;

        if (Math.abs(newX1 - x1) < eps && Math.abs(newX2 - x2) < eps && Math.abs(newX3 - x3) < eps) {
            x1 = newX1;
            x2 = newX2;
            x3 = newX3;
            break;
        }

        x1 = newX1;
        x2 = newX2;
        x3 = newX3;
    }

    const solutionDiv = document.getElementById("solution");
    solutionDiv.innerHTML = "x1 = " + roundTo(x1, 5) + "<br>" +
        "x2 = " + roundTo(x2, 5) + "<br>" +
        "x3 = " + roundTo(x3, 5);
}
function solveSystemByZeidel() {
    const eps = 0.5 * Math.pow(10, -5);
    const maxIterations = 1000;
    let x1 = 0;
    let x2 = 0;
    let x3 = 0;

    for (let i = 0; i < maxIterations; i++) {
        const oldX1 = x1;
        const oldX2 = x2;
        const oldX3 = x3;

        x1 = (b1 - a12 * oldX2 - a13 * oldX3) / a11;
        x2 = (b2 - a21 * x1 - a23 * oldX3) / a22;
        x3 = (b3 - a31 * x1 - a32 * x2) / a33;

        if (Math.abs(x1 - oldX1) < eps && Math.abs(x2 - oldX2) < eps && Math.abs(x3 - oldX3) < eps) {
            break;
        }
    }

    const solutionDiv = document.getElementById("solution");
    solutionDiv.innerHTML = "x1 = " + roundTo(x1, 5) + "<br>" +
        "x2 = " + roundTo(x2, 5) +  "<br>" +
        "x3 = " + roundTo(x3, 5);
}
